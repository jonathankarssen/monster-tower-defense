﻿using UnityEngine;
using System.Collections;

namespace MonsterTowerDefense.AI {
	public interface StateInterface<T> {
		void Enter (T entity);
		void Execute (T entity);
		void Exit(T entity);
	}
}
