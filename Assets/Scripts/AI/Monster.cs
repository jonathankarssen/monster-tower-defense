﻿using UnityEngine;
using System.Collections;

public class Monster : Unit {
	public Types.TileType tileType = Types.TileType.Normal;

	public int BuildPrice = 1;
	public int BuyPrice = 10;
	public int Level = 1;
	public bool Unlocked = false;

	public bool CanBePlacedOnNormalTile = true;

	public Monster UpgradePrefab = null;
	
	public override Unit findClosestTarget() {
		Human closest = null;
		Vector3 pos = transform.position;
		
		// find all humans
		Human[] humans = (Human[])FindObjectsOfType(typeof(Human));
		if (humans != null) {
			if (humans.Length > 0) {
				closest = humans[0];
				for (int i = 1; i < humans.Length; ++i) {
					float cur = Vector3.Distance(pos, humans[i].transform.position);
					float old = Vector3.Distance(pos, closest.transform.position);
					
					if (cur < old) {
						closest = humans[i];
					}
				}
			}
		}
		if (closest == null || Vector3.Distance (pos, closest.transform.position) >= Range) {
			closest = null;
		}
		return closest;
	}

	public void OnHit(int damage) {
		Health = Health - damage;
		if (Health <= 0) {
			Die ();
		}
	}

	public void Die ()
	{
		Node n = World.Map.FindNode (this);
		if ( n != null) 
			n.Monster = null;
		Destroy(gameObject); 
		World.Map.ForceCheck();
	}

	public Monster Upgrade ()
	{
		if (UpgradePrefab != null) {
			Vector3 pos = gameObject.transform.position;
			Destroy (gameObject);
			return (Monster) Instantiate (UpgradePrefab, pos, Quaternion.identity);
		} else
			return null;
	}

	public bool CanBuildOnSpot (Node n)
	{
		return (n.Type == tileType || (n.Type == Types.TileType.Normal && CanBePlacedOnNormalTile));
	}

	public string BuildableSpots ()
	{
		string s = tileType.ToString();
		if (CanBePlacedOnNormalTile && tileType != Types.TileType.Normal)
			s += " and normal";
		return s.ToLower();
	}

	public int GetUpgradableLevels ()
	{
		int i = 1;
		if (UpgradePrefab != null) {
			i += UpgradePrefab.GetUpgradableLevels();
		}
		return i;
	}
}