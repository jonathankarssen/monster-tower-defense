﻿using UnityEngine;
using System.Collections;
using MonsterTowerDefense.AI;

public class Unit : MonoBehaviour {
	private FiniteStateMachine<Unit> fsm;
	
	public void Awake() {
		fsm = new FiniteStateMachine<Unit>(); // initialize the FSM
		fsm.Configure(this, new ScanState()); // set the default state. Put the tower in scan mode
	}

	public void ChangeState(StateInterface<Unit> state) {
		fsm.ChangeState(state); // chances the current state to a new state
	}

	public string Name = "";
	public int Health = 100;
	public int MaxHealth = 100;
	public int Damage = 15;
	public Types.DamageType DmgType = Types.DamageType.Normal;
	public int Range = 20;

	public Unit Target = null;
	public Projectile ProjectilePrefab = null;
	
	public float AttackSpeed = 2.0f;

	public float Height = 10f;

	public virtual Unit findClosestTarget() {
		return null;
	}

	void Start() {
		Health = MaxHealth;
	}

	void Update() {
		fsm.Update();
	}
}
