﻿using UnityEngine;
using System.Collections;
using MonsterTowerDefense.AI;

public sealed class AttackState : StateInterface<Unit> {
	private float lastAttack;
	private float timeLeft = 0.0f;

	public AttackState() {
	}
	
	public void Enter (Unit unit) {
		//tower.StatusText = string.Format("Attacking enemy: {0}", target.name);
		if (unit is Human) {
			NavMeshAgent agent = unit.GetComponent<NavMeshAgent> ();
			agent.Stop ();
		}
		timeLeft = unit.AttackSpeed;
	}
	
	public void Execute (Unit tower) {
		if(tower.Target==null || (Vector3.Distance(tower.transform.position, tower.Target.transform.position) > tower.Range)) {
			tower.ChangeState(new ScanState());
			return;
		}

		//turn towards enemy
		tower.transform.LookAt (tower.Target.transform.position);
		if (tower.Name == "Whale" || tower.Name == "Allosaurus") {
			tower.transform.rotation = Quaternion.LookRotation(tower.transform.position - tower.Target.transform.position);
		}

		// shoot next projectile?
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0.0f) {
			// spawn projectile
			GameObject g = (GameObject)GameObject.Instantiate (tower.ProjectilePrefab.gameObject, tower.transform.position, Quaternion.identity);
			
			// get access to projectile component
			Projectile p = g.GetComponent<Projectile> ();
			
			// set destination        
			p.setDestination (tower.Target.transform);
			p.setDamage (tower.Damage);
			p.setType (tower.DmgType);
			
			// reset time
			timeLeft = tower.AttackSpeed;
		}
	}
	
	public void Exit (Unit tower) {
		//Debug.Log(string.Format("Target '{0}' lost... ",target.name));
		tower.Target = null;
	}
}