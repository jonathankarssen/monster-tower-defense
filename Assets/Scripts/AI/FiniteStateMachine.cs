﻿using UnityEngine;
using System.Collections;

namespace MonsterTowerDefense.AI {
	public class FiniteStateMachine <T> {
		private T Owner;
		private StateInterface<T> CurrentState;
		private StateInterface<T> PreviousState;
		private StateInterface<T> GlobalState; // The Global State is not implemented in this example, but shouldn't be to easy to add a "SetGlobalState" method which looks similar to Configure(). The Global State can be used for a state which is always executed, i.e. turning the Gun Tower in the direction of the target, if one is available there, for moving a tank while then AI script controls the turret etc. Global State is always executed (if set) when you call fsm.Update();
		
		public FiniteStateMachine() {      
			CurrentState = null;
			PreviousState = null;
			GlobalState = null;
		}
		
		public void Configure(T owner, StateInterface<T> InitialState) {
			Owner = owner;
			ChangeState(InitialState);
		}
		
		public void  Update() {
			if (GlobalState != null)  GlobalState.Execute(Owner);
			if (CurrentState != null) CurrentState.Execute(Owner);
		}
		
		public void ChangeState(StateInterface<T> NewState) {
			PreviousState = CurrentState;
			
			if (CurrentState != null)
				CurrentState.Exit(Owner);
			
			CurrentState = NewState;
			
			if (CurrentState != null)
				CurrentState.Enter(Owner);
		}
		
		public void RevertToPreviousState() {
			if (PreviousState != null)
				ChangeState(PreviousState);
		}
	};
}
