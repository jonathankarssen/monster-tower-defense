﻿using UnityEngine;
using System.Collections;

public class Human : Unit {
	public int Reward = 1;

	public override Unit findClosestTarget() {
		Monster closest = null;
		Vector3 pos = transform.position;
		
		// find all monsters
		Monster[] monsters = (Monster[])FindObjectsOfType(typeof(Monster));
		if (monsters != null) {
			if (monsters.Length > 0) {
				closest = monsters[0];
				for (int i = 1; i < monsters.Length; ++i) {
					float cur = Vector3.Distance(pos, monsters[i].transform.position);
					float old = Vector3.Distance(pos, closest.transform.position);
					
					if (cur < old) {
						closest = monsters[i];
					}
				}
			}
		}
		if (closest == null || Vector3.Distance (pos, closest.transform.position) >= Range) {
			closest = null;
		}
		return closest;
	}

	public void onHit(int damage) {
		Health = Health - damage;
		if (Health <= 0) {
			// increase player gold
			Map.gold = Map.gold + Reward;
			
			// destroy
			Destroy(gameObject); 
		}
	}
}