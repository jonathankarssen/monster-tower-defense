﻿using UnityEngine;
using System.Collections;
using MonsterTowerDefense.AI;

public sealed class ScanState : StateInterface<Unit> {
	private float scanInverval = 0.1f;
	private float lastScan;

	public ScanState() {
	}

	public void Enter (Unit unit) {
		//tower.StatusText = "Scanning for enemies...";
		if (unit is Human) {
			NavMeshAgent agent = unit.GetComponent<NavMeshAgent>();
			agent.Resume();
		}
	}
	
	public void Execute (Unit tower) {
		if(lastScan==0 || (Time.time - lastScan) >= scanInverval) {
			Unit closestTarget = tower.findClosestTarget();

			if(closestTarget!=null) {
				tower.Target = closestTarget;
				
				tower.ChangeState(new AttackState());
			}
			
			lastScan = Time.time;
		}
	}
	public void Exit (Unit tower) {
	}
}