using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
	// fly speed
	public float Speed = 10.0f;

	public int Damage = 1;
	public Types.DamageType Type = Types.DamageType.Normal;

	// destination set by Monster when creating the projectile
	public Transform Destination;    
	
	// Update is called once per frame
	void Update () {
		// destroy projectile if destination does not exist anymore
		if (Destination == null) {
			Destroy(gameObject);
			return;
		}
		
		// fly towards the destination
		float stepSize = Time.deltaTime * Speed;
		transform.position = Vector3.MoveTowards(transform.position, Destination.position, stepSize);
		
		// reached?
		if (transform.position.Equals(Destination.position)) {
			// decrease human health
			Human h = Destination.GetComponent<Human>();
			if (h != null) {
				h.onHit(Damage);
			} else {
				Monster m = Destination.GetComponent<Monster>();
				m.OnHit (Damage);
			}

			// destroy projectile
			Destroy(gameObject);
		}
	}
	
	public void setDestination(Transform v) {
		Destination = v;
	}

	public void setDamage(int dmg) {
		Damage = dmg;
	}

	public void setType(Types.DamageType t) {
		Type = t;
	}
}