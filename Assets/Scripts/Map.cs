﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class Map : MonoBehaviour {
	public GUISkin Skin;

	public List<Node> Nodes = new List<Node>();
	public Monster MonsterPrefab1 = null;
	public GameObject MonsterShell1 = null;
	
	public Monster MonsterPrefab2 = null;
	public GameObject MonsterShell2 = null;
	
	public Monster MonsterPrefab3 = null;
	public GameObject MonsterShell3 = null;

	public GameObject UnavailableSelectionPrefab = null;
	public GameObject SelectedNodePrefab = null;
	public GameObject RangeIndicatorPrefab = null;

	public MonsterList MonsterList = null;

	private Node SelectedNode = null;
	private Node spot = null;

	Vector3 coords = new Vector3(0,10,0);
	GameObject MonsterShell = null;
	Monster MonsterPrefab = null;

	public static int gold; // start gold
	public int Health = 10;

	public int MapWidth = 10;
	public int MapHeight = 10;
	public int TileSize = 10;

	public float widthPercent = 0.3f;
	public float heightPercent = 0.3f;
	
	public int NumberOfWaves = 5;
	public int CurrentWave = 1;

	private string message = "";

	private Rect monsterRect = new Rect();
	private Rect terrainRect = new Rect();
	private Rect gameOverRect = new Rect();

	private bool GameOver = false;
	private bool Victorious = false;

	void OnGUI()
	{
		GUI.skin = Skin;

		if (!GameOver) {
			Rect monsters = new Rect (0,
			                  0,
			                   Screen.width * widthPercent,
			                   Screen.height); 

			Rect current = new Rect (Screen.width - Screen.width * widthPercent,
			                   0,
			                   Screen.width * widthPercent,
			                   Screen.height); 

			monsterRect = GUI.Window (0, monsters, MonsterWindow, "");
			if (SelectedNode != null)
				terrainRect = GUI.Window (1, current, TerrainWindow, "");
		} else  {
			Rect end = new Rect ((Screen.width/2) - (Screen.width * widthPercent)/2,
			                     0, 
			                     Screen.width * widthPercent,
			                     Screen.height);
			gameOverRect = GUI.Window (2,end,EndWindow, "");
		}
	}

	private void MonsterWindow (int windowId)
	{
		GUILayout.Space (40);
		GUILayout.Label ("Shrine health: " + Health);
		GUILayout.Label ("Wave: " + CurrentWave + " of " + NumberOfWaves);
		GUILayout.Label ("Gold: " + gold);
		GUILayout.Space (20);
		if (MonsterPrefab1) {
			if (GUILayout.Button ("1. " + MonsterPrefab1.Name + " - Costs: " + MonsterPrefab1.BuildPrice))
				SetBuilding (1);
		}
		if (MonsterPrefab2) {
			if (GUILayout.Button ("2. " + MonsterPrefab2.Name + " - Costs: " + MonsterPrefab2.BuildPrice))
				SetBuilding (2);
		}
		if (MonsterPrefab3) {
			if (GUILayout.Button ("3. " + MonsterPrefab3.Name + " - Costs: " + MonsterPrefab3.BuildPrice))
				SetBuilding (3);
		}
	}

	private void TerrainWindow (int windowId)
	{
		GUILayout.Space(40);
		GUILayout.Label ("Type: " + SelectedNode.Type);
		if (SelectedNode.Monster != null)
		{
			GUILayout.Label ("Monster: " + SelectedNode.Monster.Name);
			GUILayout.Label ("Level: " + SelectedNode.Monster.Level, "ShortLabel");
			GUILayout.TextArea ("Damage: " + SelectedNode.Monster.Damage + " \n " +
			                    "Damagetype: " + SelectedNode.Monster.DmgType + " \n " +
			                    "Range: " + SelectedNode.Monster.Range + " \n " +
			                    "Health: " + SelectedNode.Monster.Health + " \n " +
			                    "Attack speed: " + (int)(60 / SelectedNode.Monster.AttackSpeed)
			                    );
			if (SelectedNode.Monster.UpgradePrefab != null)
			{
				GUILayout.Label ("------Stats after upgrade------");
				GUILayout.TextArea ("Damage: " + SelectedNode.Monster.UpgradePrefab.Damage + " \n " +
				                    "Damagetype: " + SelectedNode.Monster.UpgradePrefab.DmgType + " \n " +
				                    "Range: " + SelectedNode.Monster.UpgradePrefab.Range + " \n " +
				                    "Health: " + SelectedNode.Monster.UpgradePrefab.Health + " \n " +
				                    "Attack speed: " + 100 / SelectedNode.Monster.UpgradePrefab.AttackSpeed
				                    );
				GUILayout.Label ("Upgrade price: " + SelectedNode.Monster.UpgradePrefab.BuildPrice, "ShortLabel");
				if(GUILayout.Button ("Upgrade"))
					Upgrade(SelectedNode.Monster);
			}
		}
	}

	private void EndWindow (int windowId)
	{
		GUILayout.Space(40);
		if (Victorious) {
			AddSpikes((Screen.width/2) - (Screen.width * widthPercent)/2);
			GUILayout.Label ("Victory");
			GUILayout.TextArea ("You have defeated the teddybears! You had " + gold +
				" gold left when finishing, which you can now use to buy monsters in the shop!");
			if(GUILayout.Button ("Back to World Map"))
				Application.LoadLevel("WorldMap");
		} else {
			AddSpikes((Screen.width/2) - (Screen.width * widthPercent)/2);
			GUILayout.Label ("Defeat");
			GUILayout.TextArea ("You have been defeated. The monsters will never have a safe haven again because of you! Thanks a lot!");
			if(GUILayout.Button ("Back to World Map"))
				Application.LoadLevel("WorldMap");
		}
	}

	private void AddSpikes(float width) {
		double spikeCount = Mathf.Floor(width-152)/22;
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("", "SpikeLeft");
		for (int i = 0; i < spikeCount; i++) {
			GUILayout.Label ("", "SpikeMid");
		}
		GUILayout.Label ("", "SpikeRight");
		GUILayout.EndHorizontal ();
	}

	void Update()
	{
		if (!GameOver) {
			if (Health <= 0) {
				Defeat ();
			}

			if (Input.GetKeyDown (KeyCode.Alpha1))
				SetBuilding (1);
			else if (Input.GetKeyDown (KeyCode.Alpha2))
				SetBuilding (2);
			else if (Input.GetKeyDown (KeyCode.Alpha3))
				SetBuilding (3);

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				int x = (int)(hit.point.x - hit.point.x % 10);
				int z = (int)(hit.point.z - hit.point.z % 10);
				var n = FindNode (x, z);
				spot = n;
				if(IsBuilding())
					DrawShell ();
			}

			//build tower if clicked
			if (Input.GetButtonDown ("Fire1")) {
				if (!terrainRect.Contains (Input.mousePosition)) {
					SelectedNode = spot;
					if (IsBuilding ()) {
						BuildMonster (spot);
						SelectedNode = null;
					} else 
						DrawSelected ();
				}
			} 

			if (Input.GetButtonDown ("Fire2")) {
				StopBuildingOrSelecting();
			}

		} else {
			Time.timeScale = 0;
		}
	}

	public void StopBuildingOrSelecting(){
		spot = null;
		SelectedNode = null;
		MonsterShell = null;
		MonsterPrefab = null;
		DrawCrosses ();
		DrawSelected ();
		DrawShell ();
		ClearShellsAndExtras ();
	}

	public void Victory() 
	{
		if (!GameOver) {
			GameOver = true;
			Victorious = true;
			int playerGold = 0;
			if (PlayerPrefs.HasKey ("Gold")) {
				playerGold = PlayerPrefs.GetInt ("Gold");
			}
			playerGold = playerGold + gold;
			PlayerPrefs.SetInt ("Gold", playerGold);
		}
	}

	public void Defeat() 
	{
		GameOver = true;
		Victorious = false;
	}

	public void ResetSelection ()
	{
		spot = null;
		ForceCheck ();
	}

	public void SetBuilding (int i)
	{
		SelectedNode = null;
		if (i == 1) 
		{
			MonsterShell = MonsterShell1;
			MonsterPrefab = MonsterPrefab1;
		} 
		else if (i == 2) 
		{
			MonsterShell = MonsterShell2;
			MonsterPrefab = MonsterPrefab2;
		} 
		else if (i == 3) 
		{
			MonsterShell = MonsterShell3;
			MonsterPrefab = MonsterPrefab3;
		} 
		else if (i == 0) 
		{
			MonsterShell = null;
			MonsterPrefab = null;
		}
		DrawCrosses ();
		DrawSelected ();
		DrawShell ();
	}

	public void DrawSelected () 
	{
		ClearShellsAndExtras ();
		if (SelectedNode != null) {
			Instantiate (SelectedNodePrefab, new Vector3 ((float)(SelectedNode.x + 5), 0, (float)(SelectedNode.z + 5)), Quaternion.Euler (-90, 0, 0));
			if (SelectedNode.Monster != null) {
				RangeIndicatorPrefab.transform.localScale = new Vector3 ((float)(SelectedNode.Monster.Range * 2), 0.01f, (float)(SelectedNode.Monster.Range * 2));
				Instantiate (RangeIndicatorPrefab, SelectedNode.Monster.transform.position, Quaternion.identity);
			}
		}
	}

	public void DrawCrosses ()
	{
		foreach (GameObject o in crosses)
			o.SetActive (false);
		if (MonsterShell != null) {
			var nods = (from n in Nodes where !n.IsBuildable () || !MonsterPrefab.CanBuildOnSpot(n) select n);
			foreach(Node nod in nods) {
				GameObject go = GetInactiveCross();
				go.transform.position = new Vector3 ((float)(nod.x + 5), 0, (float)(nod.z + 5));
				go.SetActive(true);
			}
		}
	}

	private void DrawShell ()
	{
		ClearShellsAndExtras ();
		if (spot != null && MonsterPrefab != null) 
		{
			if (spot.IsBuildable () && MonsterPrefab.CanBuildOnSpot (spot)) 
			{
				coords = new Vector3 ((float)(spot.x + 5), 0, (float)(spot.z + 5));
				Instantiate (MonsterShell, coords, Quaternion.identity);
				RangeIndicatorPrefab.transform.localScale = new Vector3((float)(MonsterPrefab.Range * 2), 0.01f, (float)(MonsterPrefab.Range * 2));
				Instantiate (RangeIndicatorPrefab, coords, Quaternion.identity);
			}
		}
	}

	private void ClearShellsAndExtras(){
		foreach (GameObject g in GameObject.FindGameObjectsWithTag ("MonsterShell")) 
			Destroy (g);
		foreach (GameObject g in GameObject.FindGameObjectsWithTag("Ring"))
			Destroy(g);
	}

	public bool IsBuilding ()
	{
		return MonsterShell != null;
	}

	public void BuildMonster (Node n)
	{
		if (n != null && gold >= MonsterPrefab.BuildPrice) 
		{
			if (n.IsBuildable() && MonsterPrefab.CanBuildOnSpot(n))
			{
				gold = gold - MonsterPrefab.BuildPrice;
				Monster m = (Monster) Instantiate (MonsterPrefab, coords, Quaternion.identity);
				coords = new Vector3 (0,10,0);
				n.Monster = m;
				SetBuilding (0);
			}
		}
	}

	public void BuildSelection (){
		for (int i = 0; i < 3; i++) {
			int monsterIndex = PlayerPrefs.GetInt("Monster"+(i+1));
			SetSelectedMonster(i + 1, monsterIndex);
		}
	}

	public void BuildMap (string map, Types.TileType defaultType)
	{
		for (int x = 0; x < this.MapWidth; x++)
			for (int z = 0; z < this.MapHeight; z++)
				AddNode (defaultType, x, z);
		foreach (string s in map.Split (';')) {
			ReadNode (s);
		}
	}

	private void AddNode (Types.TileType type, int x, int z)
	{
		Node n = new Node (x * 10, z * 10, type);
		Nodes.Add (n);
	}

	private int FindNodeIndex (int x, int z)
	{
		return Nodes.FindIndex (n => n.x == x && n.z == z);
	}

	private int FindNodeIndex (Monster m)
	{
		return Nodes.FindIndex (n => n.Monster.gameObject.GetInstanceID () == m.gameObject.GetInstanceID());
	}

	private void ReadNode (string node)
	{
		string input = System.Text.RegularExpressions.Regex.Replace(node, @"^\s*$\n", string.Empty, System.Text.RegularExpressions.RegexOptions.Multiline);
		string coords = input.Split (':') [0];
		int x = (System.Convert.ToInt32 (coords.Split (',') [0]) - 1) * 10;
		int z = (System.Convert.ToInt32 (coords.Split (',') [1]) - 1) * 10;
		Node n = new Node(x, z);
		string s = input.Split (':') [1].ToLower ();
		switch (s.Trim ().ToLower ()) 
		{
		case "road" : 
			n.Type = Types.TileType.Road;
			break;
		case "normal" : 
			n.Type = Types.TileType.Normal; 
			break;
		case "earth" : 
			n.Type = Types.TileType.Earth; 
			break;
		case "water" : 
			n.Type = Types.TileType.Water;
			break;
		}
		int i = FindNodeIndex (x, z);
		Nodes [i] = n;
	}

	public Node FindNode (int x, int z)
	{
		return Nodes.Find (n => n.x == x && n.z == z);
	}

	public Node FindNode (Monster monster) 
	{
		Node n = null;
		foreach (Node node in Nodes) 
		{
			if (node.Monster != null)
				if (node.Monster.GetInstanceID () == monster.GetInstanceID ())
					n = node;
		}
		return n;
	}

	public void Upgrade (Monster monster)
	{
		if (gold >= monster.UpgradePrefab.BuildPrice) {
			gold = gold - monster.UpgradePrefab.BuildPrice;
			FindNode (monster).Monster = monster.Upgrade ();
			DrawSelected();
		} else {
			message = "Not enough Gold";
		}
	}

	public void ForceCheck ()
	{
		foreach (Node n in Nodes)
			n.ForceCheck ();
		DrawCrosses ();
		DrawSelected ();
		DrawShell ();
	}

	public void SetSelectedMonster(int index, int monster)
	{
		if (index == 1) {
			MonsterPrefab1 = MonsterList.GetMonsterAtIndex (monster);
			MonsterShell1 = MonsterList.GetMonsterShellAtIndex (monster);
		}
		if (index == 2) {
			MonsterPrefab2 = MonsterList.GetMonsterAtIndex (monster);
			MonsterShell2 = MonsterList.GetMonsterShellAtIndex (monster);
		}
		if (index == 3) {
			MonsterPrefab3 = MonsterList.GetMonsterAtIndex (monster);
			MonsterShell3 = MonsterList.GetMonsterShellAtIndex (monster);
		}
	}

	public static List<GameObject> crosses = new List<GameObject> ();
	
	public GameObject GetInactiveCross() {
		foreach (GameObject o in crosses) {
			if (!o.activeSelf) {
				return o;
			}
		}
		return null;
	}

	protected void InitList(){
		var list = Camera.main.GetComponent<MonsterList> ();
		if (list != null)
			this.MonsterList = list;
	}
}