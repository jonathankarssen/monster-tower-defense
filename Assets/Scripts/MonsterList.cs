﻿using UnityEngine;
using System.Collections;

public class MonsterList : MonoBehaviour {

	public Monster Allosaurus;
	public GameObject AllosaurusShell;
	
	public Monster Spider;
	public GameObject SpiderShell;
	
	public Monster Whale;
	public GameObject WhaleShell;
	
	public Monster SandWorm;
	public GameObject SandWormShell;
	
	public Monster IceGiant;
	public GameObject IceGiantShell;
	
	public Monster Earthborn;
	public GameObject EarthbornShell;

	public Monster GetMonsterAtIndex (int i)
	{
		if (i == Monsters.Allosaurus)
			return Allosaurus;
		if (i == Monsters.Earthborn)
			return Earthborn;
		if (i == Monsters.IceGolem)
			return IceGiant;
		if (i == Monsters.SandWorm)
			return SandWorm;
		if (i == Monsters.Spider)
			return Spider;
		if (i == Monsters.Whale)
			return Whale;
		return null;
	}
	
	public GameObject GetMonsterShellAtIndex (int i) {
		if (i == Monsters.Allosaurus)
			return AllosaurusShell;
		if (i == Monsters.Earthborn)
			return EarthbornShell;
		if (i == Monsters.IceGolem)
			return IceGiantShell;
		if (i == Monsters.SandWorm)
			return SandWormShell;
		if (i == Monsters.Spider)
			return SpiderShell;
		if (i == Monsters.Whale)
			return WhaleShell;
		return null;
	}
}
