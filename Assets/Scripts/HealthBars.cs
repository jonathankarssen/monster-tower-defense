﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealthBars : MonoBehaviour {
	public Texture2D EnemyHealthBarTexture;
	public Texture2D FriendlyHealthBarTexture;
	public int HealthBarWidth = 50;
	public int HealthBarHeight = 5;

	private List<HealthBar> EnemyBars = new List<HealthBar> ();
	private List<HealthBar> FriendlyBars = new List<HealthBar> ();
	
	// Update is called once per frame
	void Update () {
		EnemyBars.Clear();
		FriendlyBars.Clear ();
		Monster[] monsters = (Monster[])FindObjectsOfType(typeof(Monster));
		foreach (Monster h in monsters) {
			Monster monster = (Monster)h;
			float healthPercent = (float)monster.Health / (float)monster.MaxHealth;
			Vector2 playerScreen = Camera.main.WorldToScreenPoint(monster.transform.position + new Vector3(0.0f, monster.Height, 0.0f));
			float left = playerScreen.x - (HealthBarWidth / 2);
			float top = (Screen.height - playerScreen.y) + (HealthBarHeight / 2);
			FriendlyBars.Add(new HealthBar(healthPercent, left, top));
		}
		Human[] humans = (Human[])FindObjectsOfType(typeof(Human));
		foreach (Human h in humans) {
			Human human = (Human)h;
			float healthPercent = (float)human.Health / (float)human.MaxHealth;
			Vector2 playerScreen = Camera.main.WorldToScreenPoint(human.transform.position + new Vector3(0.0f, human.Height, 0.0f));
			float left = playerScreen.x - (HealthBarWidth / 2);
			float top = (Screen.height - playerScreen.y) + (HealthBarHeight / 2);
			EnemyBars.Add(new HealthBar(healthPercent, left, top));
		}

	}
	
	void OnGUI()
	{
		foreach(HealthBar hb in EnemyBars) {
			GUI.DrawTexture(new Rect(hb.Left, hb.Top, (HealthBarWidth * hb.HealthPercent), HealthBarHeight), EnemyHealthBarTexture);
		}
		foreach(HealthBar hb in FriendlyBars) {
			GUI.DrawTexture(new Rect(hb.Left, hb.Top, (HealthBarWidth * hb.HealthPercent), HealthBarHeight), FriendlyHealthBarTexture);
		}
	}

	private class HealthBar {
		public float HealthPercent;
		public float Left;
		public float Top;

		public HealthBar(float hp, float l, float top) {
			HealthPercent = hp;
			Left = l;
			Top = top;
		}
	}
}
