﻿using UnityEngine;
using System.Collections;

public class MonsterSelection : MonoBehaviour {
	public bool Selected;
	public Monster ContainingMonster = null;

	void Update() {
		if (!ContainingMonster.Unlocked) 
			this.gameObject.renderer.material.color = new Color (1, 0, 0, 0.5f);
		else 
			this.gameObject.renderer.material.color = new Color (1, 1, 0, (float)100/255);
		if (Selected)
			this.gameObject.renderer.material.color = new Color (0, 1, 0, (float)100/255);
	}

	public void UnlockMonster(){
		int gold = PlayerPrefs.GetInt ("Gold");
		if (gold >= ContainingMonster.BuyPrice) {
			ContainingMonster.Unlocked = true;
			gold = gold - ContainingMonster.BuyPrice;
			PlayerPrefs.SetInt ("Gold", gold);
		}
	}
}
