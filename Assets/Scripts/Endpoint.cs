﻿using UnityEngine;
using System.Collections;

public class Endpoint : MonoBehaviour {

	void Update() {
		// find all humans
		Human[] humans = (Human[])FindObjectsOfType(typeof(Human));
		if (humans != null) {
			// find all humans that are close to the castle
			for (int i = 0; i < humans.Length; ++i) {
				float range = Mathf.Max(collider.bounds.size.x, collider.bounds.size.z);
				if (Vector3.Distance(transform.position, humans[i].transform.position) <= range) {            
					// decrease shrine health
					World.Map.Health = World.Map.Health - 1;

					// destroy human
					Destroy(humans[i].gameObject);
				} 
			}
		}
	}
}