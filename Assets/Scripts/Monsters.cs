
public static class Monsters
{
	public const int Allosaurus = 1;
	public const int Spider = 2;
	public const int Whale = 3;
	public const int SandWorm = 4;
	public const int IceGolem = 5;
	public const int Earthborn = 6;

	public static int GetId(Monster monster){
		if (monster.name == "Allosaurus")
			return Allosaurus;
		if (monster.name == "SPIDER")
			return Spider;
		if (monster.name == "humpback_whale_model_baked_animation")
			return Whale;
		if (monster.name == "CaveWorm")
			return SandWorm;
		if (monster.name == "golem")
			return IceGolem;
		if (monster.name == "Troll")
			return Earthborn;
		return 0;
	}
}


