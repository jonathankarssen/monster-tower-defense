﻿using UnityEngine;
using System.Collections;

public class MenuItem : MonoBehaviour {
	public string ItemName;
	public string SceneName;
	public string IntroText;
	public bool IsEnabled = true;
}
