﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldMap : MonoBehaviour {

	public GUISkin Skin;
	public bool ShopOpened;
	public bool SelectingMonsters;

	private MenuItem SelectedLevel;
	private MonsterSelection SelectedMonster;
	private Rect itemRect = new Rect();
	private Rect monsterRect = new Rect();
	public float widthPercent = 0.3f;

	private List<Monster> SelectedMonsters = new List<Monster>();

	private string LoadLevel;

	void Start()
	{
		StartGame ();
		Time.timeScale = 1.0f;
	}

	void OnGUI()
	{
		GUI.skin = Skin;
		Rect monsters = new Rect (0,
		                          0,
		                          Screen.width * 0.25f,
		                          Screen.height);
		if (ShopOpened || SelectingMonsters)
			monsterRect = GUI.Window (0, monsters, MonsterWindow, "");
		else {
			Rect current = new Rect (Screen.width - Screen.width * widthPercent,
		                         0,
		                         Screen.width * widthPercent,
		                         Screen.height);

			if (SelectedLevel != null)
				itemRect = GUI.Window (1, current, LevelWindow, "");
		}
	}

	private void LevelWindow (int windowId)
	{
		GUILayout.Space(40);
		GUILayout.Label (SelectedLevel.ItemName);
		GUILayout.TextArea (SelectedLevel.IntroText);
		if (SelectedLevel.SceneName != "Shop") {
			if (GUILayout.Button ("Play")) 
				GoToSelection ();
		} else {
			if (GUILayout.Button ("Go to shop!")) 
				GoToShop ();
			if (GUILayout.Button ("Delete Save file")) 
				ResetGame ();
		}
		
		if (GUILayout.Button ("Close"))
			SelectedLevel = null;
	}

	private void MonsterWindow (int windowId)
	{
		int gold = 0;
		if (PlayerPrefs.HasKey ("Gold")) {
			gold = PlayerPrefs.GetInt ("Gold");
		} else {
			PlayerPrefs.SetInt ("Gold", 0);
		}
		GUILayout.Space (40);
		GUILayout.Label ("Gold: " + gold);
		if (GUILayout.Button ("Return to World map"))
			ResetLocation ();
		if (SelectingMonsters) {
			if (GUILayout.Button ("Begin Level"))
				StartLevel (LoadLevel);
		}
		if (SelectedMonsters.Count > 0) {
			GUILayout.Label ("Selected Monsters: ");
			string s = "";
			foreach(Monster m in SelectedMonsters) {
				s += m.Name + " \n";
			}
			GUILayout.TextArea (s);
		}
		if (SelectedMonster != null) {
			GUILayout.Label("" + SelectedMonster.ContainingMonster.Name);
			GUILayout.TextArea ("Damage: " + SelectedMonster.ContainingMonster.Damage + " \n " +
			                    "Range: " + SelectedMonster.ContainingMonster.Range + " \n " +
			                    "Health: " + SelectedMonster.ContainingMonster.MaxHealth + " \n " +
			                    "Attack speed: " + 60 / SelectedMonster.ContainingMonster.AttackSpeed + " / minute \n " +
			                    "Build price: " + SelectedMonster.ContainingMonster.BuildPrice + " gold \n " +
			                    "Can be built on: " + SelectedMonster.ContainingMonster.BuildableSpots() + " \n " + 
			                    "Levels: " + SelectedMonster.ContainingMonster.GetUpgradableLevels()
			                    );

			if(!SelectedMonster.ContainingMonster.Unlocked)
				if(GUILayout.Button("Buy it for " + SelectedMonster.ContainingMonster.BuyPrice + " gold"))
					SelectedMonster.UnlockMonster();
			
			if(SelectingMonsters && SelectedMonster.ContainingMonster.Unlocked)
			{
				if(!SelectedMonster.Selected){
					if(GUILayout.Button("Select"))
						SelectMonster(SelectedMonster);
				} else {
					if(GUILayout.Button("Deselect"))
						DeselectMonster(SelectedMonster);
				}
			}
		}
	}

	public void ResetGame(){
		PlayerPrefs.DeleteAll ();
		SelectingMonsters = false;
		ShopOpened = false;
		SelectedLevel = null;
		var mons = GameObject.FindGameObjectsWithTag ("MonsterSelection");
		foreach (GameObject ob in mons) {
			MonsterSelection ms = ob.GetComponent<MonsterSelection> ();
			ms.ContainingMonster.Unlocked = false;
		}
		StartGame ();
	}

	public void StartGame(){
		if (PlayerPrefs.GetInt ("HasStarted") == 0) {
			PlayerPrefs.SetInt("Gold", 50);
			PlayerPrefs.SetInt("HasStarted", 1);
		}
	}

	void Update()
	{
		//Make the camera scrollable if the user is in the shop.
		if (ShopOpened || SelectingMonsters) {
			float mousePosX = Input.mousePosition.x; 
			float mousePosY = Input.mousePosition.y; 
			float scrollSpeed = 30;
			float ScrollAmount = scrollSpeed * Time.deltaTime;
			int ScrollDistance = 5;

			//print ("Mouse x: " + mousePosX + "; Mouse y: " + mousePosY + "; Pos x: " + transform.position.x + "; Pos y: " + transform.position.y);
			//mouse left 
			if (((mousePosX < ScrollDistance) || (Input.GetKey(KeyCode.LeftArrow))) && (transform.position.x < 70)) { 
				transform.Translate (ScrollAmount, 0, -ScrollAmount / 2, Space.World); 
			} 
			//mouse right
			if (((mousePosX >= Screen.width - ScrollDistance) || (Input.GetKey(KeyCode.RightArrow))) && (transform.position.x > 0)) { 
				transform.Translate (-ScrollAmount, 0, ScrollAmount / 2, Space.World);  
			}
		}
		if (!monsterRect.Contains (Input.mousePosition)) {
			if (Input.GetButtonDown ("Fire1")) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit)) {
					SelectedLevel = hit.collider.gameObject.GetComponent<MenuItem> ();
					SelectedMonster = hit.collider.gameObject.GetComponent<MonsterSelection> ();
				}
			} 
		}
	}

	private void ResetLocation(){
		ShopOpened = false;
		SelectingMonsters = false;
		this.transform.localPosition = new Vector3(1361, 535, 564);
		this.transform.localRotation = Quaternion.Euler((float)71.00643, 0, 0);
		camera.fieldOfView = 86;
	}

	private void GoToShop(){
		ShopOpened = true;
		SelectingMonsters = false;
		this.transform.localPosition = new Vector3(552.8f, 99, 273.6f);
		this.transform.localRotation = Quaternion.Euler(29.1387f, -522.9323f, -364.3201f);
		camera.fieldOfView = (float)52.2366;
	}

	private void GoToSelection(){
		LoadLevel = SelectedLevel.SceneName;
		SelectingMonsters = true;
		ShopOpened = false;
		this.transform.localPosition = new Vector3(552.8f, 99, 273.6f);
		this.transform.localRotation = Quaternion.Euler(29.1387f, -522.9323f, -364.3201f);
		camera.fieldOfView = (float)52.2366;
	}

	private void SelectMonster(MonsterSelection monster){
		if (SelectedMonsters.Contains (monster.ContainingMonster))
			return;
		if (SelectedMonsters.Count < 3)
			SelectedMonsters.Add (monster.ContainingMonster);
		else 
			SelectedMonsters [2] = monster.ContainingMonster;
		CheckSelections ();
	}

	private void DeselectMonster(MonsterSelection monster) {
		foreach (Monster m in SelectedMonsters) {
			if(m.Name == monster.ContainingMonster.Name){
				monster.Selected = false;
				SelectedMonsters.Remove(m);
			}
		}
		CheckSelections ();
	}

	private void CheckSelections(){
		var mons = GameObject.FindGameObjectsWithTag ("MonsterSelection");
		foreach (GameObject ob in mons) {
			MonsterSelection ms = ob.GetComponent<MonsterSelection>();
			if (SelectedMonsters.Contains (ms.ContainingMonster))
				ms.Selected = true;
			else
				ms.Selected = false;
		}
	}

	private void StartLevel(string level)
	{
		if (SelectedMonsters.Count > 2) {
			if(SelectedMonsters[0] != null)
				PlayerPrefs.SetInt("Monster1", Monsters.GetId(SelectedMonsters[0]));
			if(SelectedMonsters[1] != null)
				PlayerPrefs.SetInt("Monster2", Monsters.GetId(SelectedMonsters[1]));
			if(SelectedMonsters[2] != null)
				PlayerPrefs.SetInt("Monster3", Monsters.GetId(SelectedMonsters[2]));
			Application.LoadLevel (LoadLevel);
		}
	}
}
