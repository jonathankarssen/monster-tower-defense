﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawn : MonoBehaviour {
	// spawn a new human each ... seconds
	public float IntervalBetweenMonsters = 3.0f;
	public float IntervalBetweenWaves = 35.0f;
	float timeLeft = 0.0f;
	int monsterNumber = 0;
	
	// gameobject to be spawned
	public GameObject Peasant = null;
	public GameObject Archer = null;
	public GameObject Boss = null;

	// destination (where all spawned objects run to)
	public Transform destination = null;

	protected bool lastWaveDone = false;

	protected List<List<GameObject>> waves = new List<List<GameObject>>();
	
	// Update is called once per frame
	void Update () {
		// time to spawn the next one?
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0.0f) {
			if (World.Map.CurrentWave <= waves.Count) {
				if (lastWaveDone == false) {
					int waveIndex = World.Map.CurrentWave - 1;
					List<GameObject> wave = waves[waveIndex];
					if (monsterNumber < wave.Count) {
						GameObject human = waves[waveIndex][monsterNumber];

						// spawn
						GameObject g = (GameObject)Instantiate(human, transform.position, Quaternion.identity);
						
						// get access to the navmesh agent component
						NavMeshAgent n = g.GetComponent<NavMeshAgent>();
						n.destination = destination.position;
						
						// reset time
						timeLeft = IntervalBetweenMonsters;

						monsterNumber++;
						print (monsterNumber);
					} else {
						if (World.Map.CurrentWave == World.Map.NumberOfWaves) {
							lastWaveDone = true;
						} else if (lastWaveDone == false) {
							//go to next wave
							World.Map.CurrentWave++;
							monsterNumber = 0;
							timeLeft = IntervalBetweenWaves;
						}
					} 
				}
			} 
			if (lastWaveDone) {
				// if all monsters dead, all waves done, send to mission complete scene
				Human[] humans = (Human[])FindObjectsOfType(typeof(Human));
				if (humans == null || humans.Length == 0) {
					World.Map.Victory();
				}
			}
		} 
	}
}