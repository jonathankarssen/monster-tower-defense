﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map3 : Map {
	// Use this for initialization
	void Start () {
		InitList ();
		BuildSelection ();
		World.Map = this;
		NumberOfWaves = 10;
		crosses = new List<GameObject>();
		for (int i = 0; i < 100; i++) {
			GameObject cross = Instantiate (UnavailableSelectionPrefab, new Vector3 (0,0,0), Quaternion.Euler (90, 0, 0)) as GameObject;
			cross.SetActive(false);
			crosses.Add (cross);
		}
		
		gold = 200;
		Health = 12;
		
		string map = "1,1:Normal; " +
			"1,2:Normal; " +
			"1,3:Normal; " +
			"2,3:Normal; " +
			"3,3:Normal; " +
			"4,3:Normal; " +
			"5,3:Normal; " +
			"3,1:Normal; " +
			"4,1:Normal; " +
			"5,1:Normal; " +
			"2,1:Road; " +
			"2,2:Road; " +
			"3,2:Road; " +
			"4,2:Road; " +
			"5,2:Road; " +
			"6,2:Road; " +
			"6,3:Road; " +
			"6,4:Road; " +
			"6,5:Road; " +
			"6,6:Road; " +
			"6,7:Road; " +
			"5,7:Road; " +
			"4,7:Road; " +
			"3,7:Road; " +
			"3,6:Road; " +
			"3,5:Road; " +
			"3,4:Road; " +
			"2,4:Road; " +
			"1,4:Road; " +
			"1,5:Road; " +
			"1,6:Road; " +
			"1,7:Road; " +
			"1,8:Road; " +
			"1,9:Road; " +
			"2,9:Road; " +
			"3,9:Road; " +
			"4,9:Road; " +
			"5,9:Road; " +
			"6,9:Road; " +
			"7,9:Road; " +
			"8,9:Road; " +
			"8,8:Road; " +
			"8,7:Road; " +
			"8,6:Road; " +
			"8,5:Road; " +
			"8,4:Road; " +
			"8,3:Road; " +
			"9,3:Road; " +
			"10,3:Road; " +
			"10,4:Road; " +
			"10,5:Road; " +
			"10,6:Road; " +
			"10,7:Road; " +
			"10,8:Road; " +
			"10,9:Road; " +
			"10,10:Road; " +
			"9,4:Water; " +
			"9,5:Water; " +
			"9,6:Water";
		
		BuildMap (map, Types.TileType.Earth);
		
	}
}