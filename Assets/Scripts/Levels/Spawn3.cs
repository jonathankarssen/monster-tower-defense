﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawn3 : Spawn {
	void Start () {
		lastWaveDone = false;
		waves.Clear();
		List<GameObject> wave1 = new List<GameObject> ();
		wave1.Add(Peasant);
		wave1.Add(Peasant);
		wave1.Add(Peasant);
		wave1.Add(Peasant);
		wave1.Add(Peasant);
		waves.Add(wave1);
		List<GameObject> wave2 = new List<GameObject> ();
		wave2.Add(Peasant);
		wave2.Add(Peasant);
		wave2.Add(Peasant);
		wave2.Add(Peasant);
		wave2.Add(Archer);
		waves.Add(wave2);
		List<GameObject> wave3 = new List<GameObject> ();
		wave3.Add(Peasant);
		wave3.Add(Peasant);
		wave3.Add(Peasant);
		wave3.Add(Archer);
		wave3.Add(Archer);
		waves.Add(wave3);
		List<GameObject> wave4 = new List<GameObject> ();
		wave4.Add(Peasant);
		wave4.Add(Peasant);
		wave4.Add(Peasant);
		wave4.Add(Archer);
		wave4.Add(Archer);
		waves.Add(wave4);
		
		List<GameObject> wave5 = new List<GameObject> ();
		wave5.Add(Peasant);
		wave5.Add(Peasant);
		wave5.Add(Peasant);
		wave5.Add(Archer);
		wave5.Add(Boss);
		waves.Add(wave5);

		waves.Add(wave2);
		waves.Add(wave1);
		waves.Add(wave2);
		waves.Add(wave4);
		List<GameObject> wave6 = new List<GameObject> ();
		wave6.Add(Peasant);
		wave6.Add(Peasant);
		wave6.Add(Archer);
		wave6.Add(Boss);
		wave6.Add(Boss);
		waves.Add(wave6);
	}
}
