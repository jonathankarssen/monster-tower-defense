using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map2 : Map {
	// Use this for initialization
	void Start () {
		InitList ();
		BuildSelection ();
		World.Map = this;
		NumberOfWaves = 7;
		crosses = new List<GameObject>();
		for (int i = 0; i < 100; i++) {
			GameObject cross = Instantiate (UnavailableSelectionPrefab, new Vector3 (0,0,0), Quaternion.Euler (90, 0, 0)) as GameObject;
			cross.SetActive(false);
			crosses.Add (cross);
		}
		
		gold = 150;
		Health = 9;

		string map = "1,6:Normal; " +
			"1,7:Normal; " +
			"1,8: Normal; " +
			"1,9: Normal; " +
			"2,6: Normal; " +
			"2,7: Normal; " +
			"2,8: Normal; " +
			"2,9:Normal; " +
			"3,1:Road; " +
			"3,2: Road; " +
			"3,3: Road; " +
			"4,3: Road; " +
			"5,3: Road; " +
			"6,3: Road; " +
			"7,3: Road; " +
			"7,4: Road; " +
			"7,5: Road; " +
			"7,6: Road; " +
			"6,6: Road; " +
			"5,6: Road; " +
			"4,6: Road; " +
			"4,7: Road; " +
			"4,8: Road; " +
			"4,9: Road; " +
			"5,9: Road; " +
			"6,9: Road; " +
			"7,9: Road; " +
			"8,9: Road; " +
			"9,9: Road; " +
			"9,8: Road; " +
			"9,7: Road; " +
			"9,6: Road; " +
			"9,5: Road; " +
			"9,4: Road; " +
			"9,3: Road; " +
			"9,2: Road; " +
			"9,1: Road; " +
			"8,3: Normal; " +
			"8,4: Normal; " +
			"8,5: Normal; " +
			"8,6: Normal; " +
			"9,10: Normal; " +
			"10,1: Normal; " +
			"10,2: Normal; " +
			"10,3: Normal; " +
			"10,4: Normal; " +
			"10,5: Normal; " +
			"10,6: Normal; " +
			"10,7: Normal; " +
			"10,8: Normal; " +
			"10,9: Normal; " +
			"10,10: Normal";

		BuildMap (map, Types.TileType.Water);
	}
}