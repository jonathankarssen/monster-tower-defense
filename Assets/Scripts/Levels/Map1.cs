using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map1 : Map {
	// Use this for initialization
	void Start () {
		InitList ();
		BuildSelection ();
		World.Map = this;
		NumberOfWaves = 5;
		crosses = new List<GameObject>();
		for (int i = 0; i < 100; i++) {
			GameObject cross = Instantiate (UnavailableSelectionPrefab, new Vector3 (0,0,0), Quaternion.Euler (90, 0, 0)) as GameObject;
			cross.SetActive(false);
			crosses.Add (cross);
		}

		gold = 100;
		Health = 7;

		string map = "5,1: Road; " +
			"5,2: Road; " +
			"5,3: Road; " +
			"5,4: Road; " +
			"5,5: Water; " +
			"5,6: Road; " +
			"5,7: Road; " +
			"5,8: Road; " +
			"5,9: Road; " +
			"5,10: Road; " +
			"6,4: Road; " +
			"6,5: Water; " +
			"6,6: Road; " +
			"7,4: Road; " +
			"7,5: Road; " +
			"7,6: Road";

		BuildMap (map, Types.TileType.Normal);
	}
}