using System;

/// <summary>
/// A node is a terrain tile.
/// </summary>
public class Node {
	/// <summary>
	/// The x. Horizontal value; left right
	/// </summary>
	public float x;
	/// <summary>
	/// The z. Vertical value; up down
	/// </summary>
	public float z;

	public Monster Monster = null;

	public Types.TileType Type;
	
	public Node(float x, float z){
		this.x = x;
		this.z = z;
		this.Type = Types.TileType.Normal;
	}
	
	public Node(float x, float z, Types.TileType type) {
		this.x = x;
		this.z = z;
		this.Type = type;
	}

	public bool IsBuildable(){
		if (Monster != null) {
			if (this.Monster.gameObject == null)
				this.Monster = null;
			else
				return false;
		}
		for (int i = (int)x - 10; i <= (int)x + 10; i = i + 10) {
			for (int j = (int)z - 10; j <= (int)z + 10; j = j + 10) {
				foreach (Node n in World.Map.Nodes) {
					if (i == n.x && j == n.z) {
						if(n.Monster != null)
							return false;
					}
				}
			}
		}
		return true;
	}

	public void ForceCheck(){
		if(this.Monster != null)
			if (this.Monster.gameObject == null)
				this.Monster = null;
	}
}


