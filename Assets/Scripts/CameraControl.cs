﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public float Speed = 10.0f;
	public float ScrollSpeed = 2.0f;

	public int ScrollDistance = 5;

	public float XMin = 0;
	public float XMax = 100;

	public float YMin = 40;
	public float YMax = 100;

	public float ZMin = 0;
	public float ZMax = 100;

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{

		float mousePosX = Input.mousePosition.x; 
		float mousePosY = Input.mousePosition.y; 
		float scrollSpeed = 2 * Camera.main.orthographicSize + 2;
		float ScrollAmount = scrollSpeed *Time.deltaTime;
		const float orthographicSizeMin = 15f;
		const float orthographicSizeMax = 256f;

		if (Input.GetAxis("Mouse ScrollWheel") < 0) // forward
		{
			if(transform.position.y < YMax)
			{
				Camera.main.orthographicSize *= 1.1f;
				transform.Translate (0,ScrollSpeed * ScrollAmount,0, Space.World); 

			}
		}
		if (Input.GetAxis("Mouse ScrollWheel") > 0) // back
		{
			if(transform.position.y > YMin)
			{
				Camera.main.orthographicSize *= 0.9f;
				transform.Translate (0,ScrollSpeed * -ScrollAmount,0, Space.World); 
			}
		}
		
		//mouse left
		if ((mousePosX < ScrollDistance) && (transform.position.x > 0))
		{ 
			transform.Translate (-ScrollAmount,0,0, Space.World); 
		} 
		//mouse right
		if ((mousePosX >= Screen.width - ScrollDistance) && (transform.position.x < 100))
		{ 
			transform.Translate (ScrollAmount,0,0, Space.World);  
		}
		//mouse down
		if ((mousePosY < ScrollDistance) && (transform.position.z > 0))
		{ 
			transform.Translate (0,0,-ScrollAmount, Space.World); 
		} 
		//mouse up
		if ((mousePosY >= Screen.height - ScrollDistance) && (transform.position.z < 100))
		{ 
			transform.Translate (0,0,ScrollAmount, Space.World); 
		}
		//Keyboard controls 
		if ((Input.GetKey(KeyCode.UpArrow)) && (transform.position.z < 100))
		{ 
			transform.Translate (0,0,ScrollAmount, Space.World); 
		} 
		if ((Input.GetKey(KeyCode.DownArrow)) && (transform.position.z > 0))
		{ 
			transform.Translate (0,0,-ScrollAmount, Space.World);
		}
		if ((Input.GetKey(KeyCode.LeftArrow)) && (transform.position.x > 0))
		{ 
			transform.Translate (-ScrollAmount,0,0, Space.World);  
		} 
		if ((Input.GetKey(KeyCode.RightArrow)) && (transform.position.x < 100))
		{ 
			transform.Translate (ScrollAmount,0,0, Space.World); 
		}
		
		Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, orthographicSizeMin, orthographicSizeMax );
	}
}
